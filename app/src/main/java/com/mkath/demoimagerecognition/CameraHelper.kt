package com.mkath.demoimagerecognition

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class CameraHelper : CoroutineScope {

    companion object {

        const val TAKE_PHOTO_REQUEST_CODE = 321
        private const val TIME_STAMP_FORMAT = "yyyyMMdd_HHmmss"

        private const val WIDTH = 1620
        private const val HEIGHT = 1080
    }

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    var pictureTaken: PictureTaken? = null

    fun takePictureWithCamera(activity: Activity) {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val context = activity.applicationContext


        val timeStamp: String =
            SimpleDateFormat(TIME_STAMP_FORMAT, Locale.getDefault()).format(Date())
        val imageName = "IMG_$timeStamp.jpg"
        val file = createFile(context, imageName)
        val lastPhotoPath =
            FileProvider.getUriForFile(context, context.packageName + ".fileprovider", file)
        pictureTaken = PictureTaken(lastPhotoPath, imageName, file.absolutePath)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, lastPhotoPath)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        activity.startActivityForResult(
            intent,
            TAKE_PHOTO_REQUEST_CODE
        )
    }

    private fun createFile(context: Context, imageName: String): File {
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File(storageDir, imageName)
    }

    fun deleteFile(photoPath: Uri?, context: Context) {
        photoPath?.let {
            context.contentResolver.delete(photoPath, null, null)
        }
    }

    fun changeBitmapResolution(context: Context) {
        launch {
            runChangeBitmapResolution(context)
        }
    }


    private suspend fun runChangeBitmapResolution(context: Context) =
        withContext(Dispatchers.Default) {
            return@withContext coroutineScope {
                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.ARGB_8888
                val bitmap = BitmapFactory.decodeFile(pictureTaken?.absolutePath, options)
                saveBitmapToFile(bitmap, context)
            }
        }

    fun getPath(): String{
        return pictureTaken!!.absolutePath
    }

    private fun saveBitmapToFile(originalBitmap: Bitmap, context: Context) {
        pictureTaken?.nameOfPicture?.let {
            val file = createFile(context, it)
            val fileOutputStream: OutputStream = FileOutputStream(file)
            val scaledBitmap = Bitmap.createScaledBitmap(originalBitmap, WIDTH/2, HEIGHT/2, true)
            val rotatedBitmap = rotateBitmap(scaledBitmap)
            val stream = ByteArrayOutputStream()
            rotatedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val byteArray = stream.toByteArray()
            fileOutputStream.write(byteArray)
            fileOutputStream.flush()
            fileOutputStream.close()
            rotatedBitmap?.recycle()
        }
    }

    fun getBitmap(): Bitmap {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        val bitmap = BitmapFactory.decodeFile(pictureTaken?.absolutePath, options)
        val file = File(pictureTaken!!.absolutePath)
        val fileOutputStream: OutputStream = FileOutputStream(file)
        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, WIDTH/2, HEIGHT/2, true)
        val rotatedBitmap = rotateBitmap(scaledBitmap)
        val stream = ByteArrayOutputStream()
        rotatedBitmap?.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        val byteArray = stream.toByteArray()
        fileOutputStream.write(byteArray)
        fileOutputStream.flush()
        fileOutputStream.close()
        rotatedBitmap?.recycle()
        return bitmap
    }


    private fun rotateBitmap(scaledBitmap: Bitmap): Bitmap? {
        pictureTaken?.absolutePath?.let {
            val exif = ExifInterface(it)
            val rotation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )
            val rotationInDegrees = exifToDegrees(rotation)
            val matrix = Matrix()
            if (rotation != 0) {
                matrix.preRotate(rotationInDegrees.toFloat())
            }
            return Bitmap.createBitmap(
                scaledBitmap,
                0,
                0,
                scaledBitmap.width,
                scaledBitmap.height,
                matrix,
                true
            )
        }
        return null
    }

    private fun exifToDegrees(exifOrientation: Int): Int {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270
        }
        return 0
    }
}
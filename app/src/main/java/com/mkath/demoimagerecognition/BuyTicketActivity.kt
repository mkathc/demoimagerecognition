package com.mkath.demoimagerecognition

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.graphics.scale
import com.bumptech.glide.Glide
import com.mkath.demoimagerecognition.data.network.BodyCreateResponse
import com.mkath.demoimagerecognition.data.network.ServiceFactory
import kotlinx.android.synthetic.main.activity_entry_data.*
import kotlinx.android.synthetic.main.activity_entry_data.btnBuy
import kotlinx.android.synthetic.main.activity_entry_data.etCelular
import kotlinx.android.synthetic.main.activity_entry_data.etCorreo
import kotlinx.android.synthetic.main.activity_entry_data.etDni
import kotlinx.android.synthetic.main.activity_entry_data.etName
import kotlinx.android.synthetic.main.activity_entry_data.imPhoto
import kotlinx.android.synthetic.main.activity_entry_data.spEvento
import kotlinx.android.synthetic.main.activity_entry_data.spNumeroEntradas

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class BuyTicketActivity : AppCompatActivity() {

    private val permissionHelper = PermissionHelper()
    private val cameraHelper = CameraHelper()
    private var tickets: String = ""
    private val listOfEvents = arrayOf("Partido Amistoso", "Panamericanos", "Concierto de Rock")
    private val listOfZones = arrayOf("Partido Amistoso", "Panamericanos", "Concierto de Rock")
    private val listOfNumTickets = arrayOf("1", "2", "3", "4")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry_data)
        tvHintEvento.setOnClickListener {
            setViewBehaviorEvents()
            spEvento.performClick()
        }

        tvHintZonas.setOnClickListener {
            setViewBehaviorZones()
            spZonas.performClick()
        }

        relativeLayoutNumeroEntradas.setOnClickListener {
            spNumeroEntradas.visibility = View.VISIBLE
            setListTickets()
            spNumeroEntradas.performClick()

        }

        btnFoto.setOnClickListener {
            openCamera()
        }


        imPhoto.setOnClickListener {
            openCamera()
        }


        btnBuy.setOnClickListener { sendDataBuyTicket()}
    }

    private fun setViewBehaviorEvents() {
        spEvento.visibility = View.VISIBLE
        tvTopTitleEvento.visibility = View.VISIBLE
        tvHintEvento.visibility = View.GONE
        setListEvents()
    }

    private fun setViewBehaviorZones() {
        spZonas.visibility = View.VISIBLE
        tvTopTitleZona.visibility = View.VISIBLE
        tvHintZonas.visibility = View.GONE
        setListZones()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionHelper.handlePermissionResult(requestCode, permissions, grantResults, listener = {
            if (it) {
                cameraHelper.takePictureWithCamera(this)
            }
        })
    }

    private fun setListEvents(){
        val arrayAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, listOfEvents)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spEvento.adapter = arrayAdapter
        spEvento.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItemEvent = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun setListZones(){
        val arrayAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, listOfZones)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spZonas.adapter = arrayAdapter
        spZonas.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItemZone = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun setListTickets(){
        val arrayAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, listOfNumTickets)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spNumeroEntradas.adapter = arrayAdapter
        spNumeroEntradas.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItemTicket = parent.getItemAtPosition(position).toString()
                tickets = selectedItemTicket
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CameraHelper.TAKE_PHOTO_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                cameraHelper.changeBitmapResolution(applicationContext)
                imPhoto.visibility = View.VISIBLE
                btnFoto.visibility = View.GONE
                Glide.with(this).load(cameraHelper.getBitmap()).into(imPhoto)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                cameraHelper.deleteFile(
                    cameraHelper.pictureTaken?.lastPhotoPath,
                    applicationContext
                )
            }
        }
    }

    private fun openCamera() {
        val hasPermission = permissionHelper.askForPermissions(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (hasPermission) {
            cameraHelper.takePictureWithCamera(this)
        }
    }

    private fun sendDataBuyTicket(){
        val service =
            ServiceFactory.makeService(BuildConfig.DEBUG)
        val file = File(cameraHelper.getPath())
        val requestFile =
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file
            )
        val body =
            MultipartBody.Part.createFormData("image", file.getName(), requestFile)

        val fullname = RequestBody.create(
            MultipartBody.FORM, etName.text.toString()
        )

        val correo = RequestBody.create(
            MultipartBody.FORM, etCorreo.text.toString()
        )

        val celular = RequestBody.create(
            MultipartBody.FORM, etCelular.text.toString()
        )
        val dni = RequestBody.create(
            MultipartBody.FORM, etDni.text.toString()
        )
        val ticket = RequestBody.create(
            MultipartBody.FORM, tickets
        )

        val call: Call<BodyCreateResponse> = service.createUser(body,
            fullname , correo, celular, dni, ticket )

        call.enqueue(object : Callback<BodyCreateResponse> {
            override fun onResponse(call: Call<BodyCreateResponse>?, response: Response<BodyCreateResponse>?) {
                if (response!!.code() == 400) {
                    Toast.makeText(applicationContext, "Error de procesamiento", Toast.LENGTH_SHORT)
                        .show()
                }else{
                    if (response!!.code() == 200){
                        Toast.makeText(applicationContext, "Usuario Creado con éxito", Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            }

            override fun onFailure(call: Call<BodyCreateResponse>?, t: Throwable?) {
                Toast.makeText(applicationContext, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

}

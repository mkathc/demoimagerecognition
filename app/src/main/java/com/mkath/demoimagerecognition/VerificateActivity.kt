package com.mkath.demoimagerecognition

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.mkath.demoimagerecognition.data.network.BodyVerificateResponse
import com.mkath.demoimagerecognition.data.network.ServiceFactory
import kotlinx.android.synthetic.main.activity_verificate.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class VerificateActivity : AppCompatActivity() {
    private val permissionHelper = PermissionHelper()
    private val cameraHelper = CameraHelper()
    private val listOfZones = arrayOf("Partido Amistoso", "Panamericanos", "Concierto de Rock")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verificate)
        btnVerificate.setOnClickListener {
            openCamera()

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionHelper.handlePermissionResult(requestCode, permissions, grantResults, listener = {
            if (it) {
                cameraHelper.takePictureWithCamera(this)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CameraHelper.TAKE_PHOTO_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                cameraHelper.changeBitmapResolution(applicationContext)
                uploadFile(cameraHelper.getPath())
            } else if (resultCode == Activity.RESULT_CANCELED) {
                cameraHelper.deleteFile(
                    cameraHelper.pictureTaken?.lastPhotoPath,
                    applicationContext
                )
            }
        }
    }

    private fun uploadFile(path: String) {
        val service =
        ServiceFactory.makeService(BuildConfig.DEBUG)
        val file = File(path)
        val requestFile =
        RequestBody.create(
            MediaType.parse("multipart/form-data"),
            file
        )
        val body =
        MultipartBody.Part.createFormData("image", file.getName(), requestFile)
        val call: Call<BodyVerificateResponse> = service.verificateUser(body)
        call.enqueue(object : Callback<BodyVerificateResponse> {
            override fun onResponse(call: Call<BodyVerificateResponse>?, response: Response<BodyVerificateResponse>?) {
                Toast.makeText(applicationContext, "CORRECT", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<BodyVerificateResponse>?, t: Throwable?) {
                Toast.makeText(applicationContext, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun openCamera() {
        val hasPermission = permissionHelper.askForPermissions(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (hasPermission) {
            cameraHelper.takePictureWithCamera(this)
        }
    }
}


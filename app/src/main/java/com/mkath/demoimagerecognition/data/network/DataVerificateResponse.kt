package com.mkath.demoimagerecognition.data.network
import com.google.gson.annotations.SerializedName


class DataVerificateResponse(
    @SerializedName("msg")
    val msg: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("cellphone")
    val cellphone: String,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("zone")
    val zone: String,
    @SerializedName("num_tickets")
    val num_tickets: Int
)

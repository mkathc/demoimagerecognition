package com.mkath.demoimagerecognition.data.network

import android.media.Image
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path

interface DataServices {
    @Multipart
    @POST("create-user")
    fun createUser(@Part create: MultipartBody.Part,
                   @Part("fullname")  fullname: RequestBody,
                   @Part("email")  email:RequestBody,
                   @Part("cellphone")  cellphone:RequestBody,
                   @Part("dni")  dni:RequestBody,
                   @Part("num_tickets")  num_tickets:RequestBody): Call<BodyCreateResponse>

    @Multipart
    @POST("verificate-user")
    fun verificateUser(@Part image: MultipartBody.Part): Call<BodyVerificateResponse>
}

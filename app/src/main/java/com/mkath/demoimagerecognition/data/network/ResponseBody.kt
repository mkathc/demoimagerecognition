package com.mkath.demoimagerecognition.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class ResponseBody {
    @SerializedName("status")
    @Expose
    val status: Boolean? = null

    @SerializedName("code")
    @Expose
    val code: Int? = null

    @SerializedName("message")
    @Expose
    val message: String? = null
}
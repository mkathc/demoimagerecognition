package com.mkath.demoimagerecognition.data.network
import com.google.gson.annotations.SerializedName


class BodyCreateResponse(
    @SerializedName("details")
    val details: ArrayList<DataCreateResponse>
)

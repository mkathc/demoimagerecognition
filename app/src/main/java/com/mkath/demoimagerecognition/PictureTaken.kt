package com.mkath.demoimagerecognition

import android.net.Uri

class PictureTaken(val lastPhotoPath: Uri, val nameOfPicture: String, val absolutePath: String)